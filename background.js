chrome.runtime.onInstalled.addListener(() => {
    // Auto start badge
    chrome.cookies.set(
        {"name": "snowflake-allow", "secure": true, "sameSite": "no_restriction", "url": "https://snowflake.torproject.org", "value": "1", "expirationDate": 2145916800 });
    openBadge();
});

chrome.runtime.onStartup.addListener(() => {
    isBadgeOpen().then((isOpen) => {
        if (!isOpen) {
            openBadge();
        }
    });
});

function openBadge() {
  chrome.tabs.create({
      url: "https://snowflake.torproject.org/embed.html",
      pinned: true
  });
}

function isBadgeOpen() {
    return new Promise((resolve, reject) => {
        chrome.tabs.query({
            url: "https://snowflake.torproject.org/embed.html"
        }).then((result) => {
            if (result.length === 0) {
                resolve(false);
            } else { resolve(true); }
        });
    });
}

// If the snowflake badge tab is closed, reopen it
chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
    isBadgeOpen().then((isOpen) => {
        if (!isOpen) {
            openBadge();
        }
    });
});
